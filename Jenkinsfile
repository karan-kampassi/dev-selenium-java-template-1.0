def getFolderName() {
  def array = pwd().split("/")
  return array[array.length - 2];
}
pipeline {
  agent any
  environment {
    BRANCHES = "${env.GIT_BRANCH}"
    COMMIT = "${env.GIT_COMMIT}"
    RELEASE_NAME = "selenium"
    SERVICE_PORT = "${APP_PORT}"
    DOCKERHOST = "${DOCKERHOST_IP}"
    REGISTRY_URL = "${DOCKER_REPO_URL}"
    ACTION = "${ACTION}"

    DEPLOYMENT_TYPE = "${DEPLOYMENT_TYPE == ""? "EC2":DEPLOYMENT_TYPE}"
    KUBE_SECRET = "${KUBE_SECRET}"

    foldername = getFolderName()

    CHROME_BIN = "/usr/bin/google-chrome"
    ARTIFACTORY = "${ARTIFACTORY == ""? "ECR":ARTIFACTORY}"
    ARTIFACTORY_CREDENTIALS = "${ARTIFACTORY_CREDENTIAL_ID}"

  }

  stages {
    stage('init') {
      steps {
        script {

          def job_name = "$env.JOB_NAME"
          print(job_name)
          def values = job_name.split('/')
          namespace_prefix = values[0].replaceAll("[^a-zA-Z0-9]+","").toLowerCase().take(50)
          namespace = "$namespace_prefix-$env.foldername".toLowerCase()
          service = values[2].replaceAll("[^a-zA-Z0-9]+","").toLowerCase().take(50)
          print("kube namespace: $namespace")
          print("service name: $service")
          env.namespace_name=namespace
          env.service=service

        }
      }
    }

    stage('Deploy') {
      when {
        expression {
          env.ACTION == 'DEPLOY'
        }
      }

      steps {
        script {
          if (env.DEPLOYMENT_TYPE == 'EC2') {

            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker pull dosel/zalenium"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker stop ${JOB_BASE_NAME} || true && docker rm ${JOB_BASE_NAME} || true"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker run -d --name ${JOB_BASE_NAME} -p $SERVICE_PORT:4444 -e PULL_SELENIUM_IMAGE=true -v /var/run/docker.sock:/var/run/docker.sock  -v /tmp/videos:/home/seluser/videos --privileged  dosel/zalenium start --timeZone "UTC""'

            env.REMOTE_DRIVER_HOST = "http://$DOCKERHOST:$SERVICE_PORT"

          }
          if (env.DEPLOYMENT_TYPE == 'KUBERNETES') {

            if (env.ARTIFACTORY == 'JFROG' || env.ARTIFACTORY == 'ACR') {
                withCredentials([file(credentialsId: "$KUBE_SECRET", variable: 'KUBECONFIG'), usernamePassword(credentialsId: "$ARTIFACTORY_CREDENTIALS", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                  sh '''
                    kubectl create ns "$namespace_name" || true
                    kubectl -n "$namespace_name" create secret docker-registry regcred --docker-server="$REGISTRY_URL" --docker-username="$USERNAME" --docker-password="$PASSWORD" || true
                  '''
              }
            }
            withCredentials([file(credentialsId: "$KUBE_SECRET", variable: 'KUBECONFIG')]) {
                  sh '''
                    kubectl create ns "$namespace_name" || true
                    helm upgrade --install $RELEASE_NAME -n "$namespace_name" zalenium --atomic --timeout 300s
                    sleep 10
                  '''
                  script {
                    env.temp_service_name = "$RELEASE_NAME-zalenium".take(63)
                    def url = sh (returnStdout: true, script: '''kubectl get svc -n "$namespace_name" | grep "$temp_service_name" | awk '{print $4}' ''').trim()

                    if (url != "<pending>") {
                      env.REMOTE_DRIVER_HOST = "http://$url"
                      print("##\$@\$ http://$url/dashboard ##\$@\$")
                    }
                  }
            }

          }
        }
      }
    }

    stage('run maven') {
      steps {
        script{
          sh '''
              sleep 60
              mvn clean install -DREMOTE_DRIVER_HOST="$REMOTE_DRIVER_HOST"
          '''
        }
      }
    }

    stage('Destroy') {
      when {
        expression {
          env.DEPLOYMENT_TYPE == 'EC2' && env.ACTION == 'DESTROY'
        }
      }
      steps {
        script {
          if (env.DEPLOYMENT_TYPE == 'EC2') {
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker stop ${JOB_BASE_NAME} || true && docker rm ${JOB_BASE_NAME} || true"'
          }
          if (env.DEPLOYMENT_TYPE == 'KUBERNETES') {
            withCredentials([file(credentialsId: "$KUBE_SECRET", variable: 'KUBECONFIG')]) {
                  sh '''
                    helm uninstall $RELEASE_NAME -n "$namespace_name"
                  '''
            }
          }
        }
      }
    }
  }
}
